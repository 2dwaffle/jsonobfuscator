package com.obfuscator.models;

public class CommandOptions {
    private String inputJsonPath;
    private String outputJsonPath;
    private String outputMapPath;

    public CommandOptions(String inputJsonPath, String outputFilePath, String outputMapPath) {
        this.inputJsonPath = inputJsonPath;
        this.outputJsonPath = outputFilePath;
        this.outputMapPath = outputMapPath;
    }

    public String getInputJsonPath() {
        return inputJsonPath;
    }

    public String getOutputJsonPath() {
        return outputJsonPath;
    }

    public String getOutputMapPath() {
        return outputMapPath;
    }

    public static CommandOptions empty(){
        return new CommandOptions("", "", "");
    }

    public static boolean isEmpty(CommandOptions cmdOptions){
        return (cmdOptions.inputJsonPath.isEmpty() && cmdOptions.outputJsonPath.isEmpty() && cmdOptions.outputMapPath.isEmpty());
    }

}
