package com.obfuscator.models;

import java.util.Map;

public class ObfuscatedLine {
    private String value;
    private Map<String, String> obfuscationMap;

    public ObfuscatedLine(String value, Map<String, String> jsonStringMaps){
        this.value = value;
        this.obfuscationMap = jsonStringMaps;
    }

    public String getValue() {
        return value;
    }

    public Map<String, String> getObfuscationMap() {
        return obfuscationMap;
    }
}
