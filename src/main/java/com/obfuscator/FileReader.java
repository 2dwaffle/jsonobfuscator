package com.obfuscator;

import java.io.*;

public class FileReader {
    private BufferedReader bufferedReader;

    public FileReader(String jsonPath) throws FileNotFoundException {
        bufferedReader = new BufferedReader(new java.io.FileReader(jsonPath));
    }

    public String getNextLine() throws IOException {
        String line;
        if ((line = bufferedReader.readLine()) != null){
            return line;
        }

        closeStreaming();
        return null;
    }

    private void closeStreaming() throws IOException{
        if (bufferedReader != null){
            bufferedReader.close();
        }
    }
}
