package com.obfuscator;

import com.obfuscator.models.CommandOptions;
import com.obfuscator.models.ObfuscatedLine;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args)  {
        try {
            var commandOptionsParser = new CommandOptionsParser();
            var commandOptions = commandOptionsParser.parse(args);
            if (CommandOptions.isEmpty(commandOptions)){
                return;
            }

            var jsonReader = new FileReader(commandOptions.getInputJsonPath());
            var obfuscator = new Obfuscator();
            var obfuscatedJsonWriter = new FileWriter(commandOptions.getOutputJsonPath());
            //NOTE: tested with huge file that has more than 200k different values - obfuscation map
            //      takes a lot of RAM. For the future - use Sqlite instead?
            var obfuscationMap = new LinkedHashMap<String, String>();
            ObfuscatedLine obfuscatedLine;

            var fileLine = jsonReader.getNextLine();
            while (fileLine != null) {
                obfuscatedLine = obfuscator.obfuscate(fileLine);
                obfuscatedJsonWriter.writeLine(obfuscatedLine.getValue());
                fileLine = jsonReader.getNextLine();

                updateObfuscationMap(obfuscationMap, obfuscatedLine);
            }

            obfuscatedJsonWriter.closeWriter();

            writeObfuscationMapToFile(commandOptions.getOutputMapPath(), obfuscationMap);
        } catch (ParseException e){
            System.out.println(e.getMessage());
            return;
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static void updateObfuscationMap(Map<String, String> obfuscationMap, ObfuscatedLine obfuscatedLine) {
        for (Map.Entry<String, String> entry : obfuscatedLine.getObfuscationMap().entrySet()){
            var key = entry.getKey();
            var value = entry.getValue();

            obfuscationMap.putIfAbsent(key, value);
        }
    }

    private static void writeObfuscationMapToFile(String outputMapPath, Map<String, String> obfuscationMap) throws IOException{
        var obfuscationMapWriter = new FileWriter(outputMapPath);
        for (Map.Entry<String, String> entry : obfuscationMap.entrySet()){
            var key = entry.getKey();
            var value = entry.getValue();
            var outputLine = String.format("\"%s\" = \"%s\"", key, value);

            obfuscationMapWriter.writeLine(outputLine);
        }

        obfuscationMapWriter.closeWriter();
    }
}
