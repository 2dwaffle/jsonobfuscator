package com.obfuscator;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileWriter {
    private BufferedWriter bufferedWriter;

    public FileWriter(String filePath) throws IOException {
        createFileIfNotExists(filePath);

        bufferedWriter = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(filePath, false), StandardCharsets.UTF_8
        ));
    }

    public void writeLine(String line) throws IOException {
        bufferedWriter.write(line);
        bufferedWriter.newLine();
    }

    public void closeWriter() throws IOException {
        bufferedWriter.close();
    }

    private void createFileIfNotExists(String filePath) throws IOException {
        var file = new File(filePath);
        file.getParentFile().mkdirs();
        file.createNewFile();
    }
}
