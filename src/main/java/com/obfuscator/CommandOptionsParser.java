package com.obfuscator;

import com.obfuscator.models.CommandOptions;
import org.apache.commons.cli.*;

public class CommandOptionsParser {
    public CommandOptions parse(String[] arguments) throws ParseException {
        var options = new Options();
        //TODO: add map output file
        options.addOption("inputJsonPath", true, "Path to JSON file that will be obfuscated");
        options.addOption("outputJsonPath", true, "Path to desired output location of obfuscated JSON file (file will be created automatically)");
        options.addOption("outputMapPath", true, "Path to desired output location of obfuscation mapping file (file will be created automatically))");
        options.addOption("h", "help", false, "Prints available options");

        CommandLineParser parser = new DefaultParser();

            var cmd = parser.parse(options, arguments);

            if (cmd.hasOption("h")) {
                var helpFormatter = new HelpFormatter();
                helpFormatter.printHelp("ant", options);

                return CommandOptions.empty();
            }

            if (!validateCmdOptions(cmd)) {
                return CommandOptions.empty();
            }

            return new CommandOptions(cmd.getOptionValue("inputJsonPath"), cmd.getOptionValue("outputJsonPath"), cmd.getOptionValue("outputMapPath"));
    }

    private boolean validateCmdOptions(CommandLine cmd) {
        var areOptionsPresent = true;
        if (!cmd.hasOption("inputJsonPath")){
            System.out.println("Missing command line argument 'inputJsonPath'");
            areOptionsPresent = false;
        }

        if (!cmd.hasOption("outputJsonPath")){
            System.out.println("Missing command line argument 'outputJsonPath'");
            areOptionsPresent = false;
        }

        if (!cmd.hasOption("outputMapPath")){
            System.out.println("Missing command line argument 'outputMapPath'");
            areOptionsPresent = false;
        }

        return areOptionsPresent;
    }
}
