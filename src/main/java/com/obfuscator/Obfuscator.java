package com.obfuscator;

import com.obfuscator.models.ObfuscatedLine;

import java.security.InvalidParameterException;
import java.util.LinkedHashMap;

public class Obfuscator {
    public ObfuscatedLine obfuscate(String fileLine) throws InvalidParameterException {
        if (!fileLine.contains("\"")) {
            return new ObfuscatedLine(fileLine, new LinkedHashMap<>());
        }

        var numberOfQuotes = fileLine.length() - fileLine.replace("\"", "").length();
        if ((numberOfQuotes & 1) != 0) {
            throw new InvalidParameterException("JSON is invalid - number of quotation marks is odd");
        }

        var lineBuffer = new StringBuilder(fileLine);
        var openingQuoteIndex = 0;
        var closingQuoteIndex = 0;
        var lastQuoteIndex = lineBuffer.lastIndexOf("\"");
        var obfuscatedStringBuilder = new StringBuilder();
        var obfuscationMap = new LinkedHashMap<String, String>();

        while (closingQuoteIndex < lastQuoteIndex) {
            openingQuoteIndex = lineBuffer.indexOf("\"", closingQuoteIndex > 0 ? closingQuoteIndex + 1 : closingQuoteIndex);
            closingQuoteIndex = lineBuffer.indexOf("\"", openingQuoteIndex + 1);

            var jsonString = lineBuffer.substring(openingQuoteIndex + 1, closingQuoteIndex);

            for (char c : jsonString.toCharArray()){
                obfuscatedStringBuilder.append("\\u").append(String.format("%04x", (int)  c));
            }

            var obfuscatedString = obfuscatedStringBuilder.toString();

            lineBuffer.replace(openingQuoteIndex + 1, closingQuoteIndex, obfuscatedString);
            closingQuoteIndex = lineBuffer.indexOf("\"", openingQuoteIndex + 1);
            lastQuoteIndex = lineBuffer.lastIndexOf("\"");

            obfuscationMap.putIfAbsent(jsonString, obfuscatedString);

            obfuscatedStringBuilder.setLength(0);
        }

        return new ObfuscatedLine(lineBuffer.toString(), obfuscationMap);
    }
}
