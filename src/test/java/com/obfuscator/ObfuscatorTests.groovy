package com.obfuscator

import com.obfuscator.models.ObfuscatedLine
import org.junit.Test

import java.security.InvalidParameterException

class ObfuscatorTests extends GroovyTestCase {
    private Obfuscator obfuscator;

    public ObfuscatorTests(){
        obfuscator = new Obfuscator();
    }

    @Test
    public void test_tryObfuscateLine_LineDoesNotContainString_ReturnsUntouched(){
        //Arrange
        String line = "{";

        //Act
        ObfuscatedLine obfuscatedLine = obfuscator.obfuscate(line);

        //Assert
        assertEquals("{", obfuscatedLine.value);
        assertEquals(0, obfuscatedLine.obfuscationMap.size());
    }

    @Test
    public void test_tryObfuscateLine_LineHasOddQuotationMarksNumber_Throws() throws InvalidParameterException {
        //Arrange
        String line = "\"A string";
        String message;

        //Act
        try {
            obfuscator.obfuscate(line);
        } catch (InvalidParameterException e){
            message = e.message;
        }

        assertEquals("JSON is invalid - number of quotation marks is odd", message)
    }

    @Test
    public void test_tryObfuscateLine_LineWithSingleJsonString_ReturnsObfuscatedLine() {
        //Arrange
        String line = "\"String\": 1";
        List<String> keys = Arrays.asList("String");
        List<String> values = Arrays.asList("\\u0053\\u0074\\u0072\\u0069\\u006e\\u0067");

        //Act
        ObfuscatedLine obfuscatedLine = obfuscator.obfuscate(line);

        //Assert
        assertEquals("\"\\u0053\\u0074\\u0072\\u0069\\u006e\\u0067\": 1", obfuscatedLine.value);
        assertEquals(1, obfuscatedLine.obfuscationMap.size())
        assertObfuscationMap(obfuscatedLine.getObfuscationMap(), keys, values);
    }

    public void test_tryObfuscateLine_LineWithTwoJsonStrings_ReturnsObfuscatedLine() {
        //Arrange
        String line = "\"String\": \"1\"";
        List<String> keys = Arrays.asList("String", "1");
        List<String> values = Arrays.asList("\\u0053\\u0074\\u0072\\u0069\\u006e\\u0067", "\\u0031");

        //Act
        ObfuscatedLine obfuscatedLine = obfuscator.obfuscate(line);

        //Assert
        assertEquals("\"\\u0053\\u0074\\u0072\\u0069\\u006e\\u0067\": \"\\u0031\"", obfuscatedLine.value);
        assertEquals(2, obfuscatedLine.obfuscationMap.size())
        assertObfuscationMap(obfuscatedLine.getObfuscationMap(), keys, values);
    }

    public void test_tryObfuscateLine_LineWithMultipleJsonStrings_ReturnsObfuscatedLine() {
        //Arrange
        String line = "\"String\": [\"1\", \"two\", \"003\", \"four\"]";
        List<String> keys = Arrays.asList("String", "1", "two", "003", "four");
        List<String> values = Arrays.asList("\\u0053\\u0074\\u0072\\u0069\\u006e\\u0067", "\\u0031", "\\u0074\\u0077\\u006f", "\\u0030\\u0030\\u0033", "\\u0066\\u006f\\u0075\\u0072");

        //Act
        ObfuscatedLine obfuscatedLine = obfuscator.obfuscate(line);

        //Assert
        assertEquals("\"\\u0053\\u0074\\u0072\\u0069\\u006e\\u0067\": [\"\\u0031\", \"\\u0074\\u0077\\u006f\", \"\\u0030\\u0030\\u0033\", \"\\u0066\\u006f\\u0075\\u0072\"]", obfuscatedLine.value);
        assertEquals(5, obfuscatedLine.obfuscationMap.size())
        assertObfuscationMap(obfuscatedLine.getObfuscationMap(), keys, values);
    }

    private void assertObfuscationMap(Map<String, String> obfuscationMap, List<String> keys, List<String> values){
        int currentEntry = 0;

        for (Map.Entry<String, String> entry : obfuscationMap.entrySet()) {
            assertEquals(keys[currentEntry], entry.getKey());
            assertEquals(values[currentEntry], entry.getValue())

            currentEntry++;
        }
    }
}
