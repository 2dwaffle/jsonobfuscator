package com.obfuscator

import com.obfuscator.models.CommandOptions;
import org.junit.Test

class CommandOptionsParserTest extends GroovyTestCase {
    private CommandOptionsParser _commandOptionsParser;

    public CommandOptionsParserTest(){
        _commandOptionsParser = new CommandOptionsParser();
    }

    @Test
    public void test_parse_InputJsonFilePathNotGiven_ReturnsEmptyCommandOptions(){
        //Arrange
        String[] suppliedCommandOptions = new String[4];
        suppliedCommandOptions[0] = "-outputJsonPath";
        suppliedCommandOptions[1] = "C:\\Dir0";
        suppliedCommandOptions[2] = "-outputMapPath";
        suppliedCommandOptions[3] = "C:\\Dir1";

        //Act
        CommandOptions commandOptions = _commandOptionsParser.parse(suppliedCommandOptions);

        //Assert
        assertTrue(CommandOptions.isEmpty(commandOptions));
    }

    @Test
    public void test_parse_OutputJsonFilePathNotGiven_ReturnsEmptyCommandOptions(){
        //Arrange
        String[] suppliedCommandOptions = new String[4];
        suppliedCommandOptions[0] = "-inputJsonPath";
        suppliedCommandOptions[1] = "C:\\Dir0";
        suppliedCommandOptions[2] = "-outputMapPath";
        suppliedCommandOptions[3] = "C:\\Dir1";

        //Act
        CommandOptions commandOptions = _commandOptionsParser.parse(suppliedCommandOptions);

        //Assert
        assertTrue(CommandOptions.isEmpty(commandOptions));
    }

    @Test
    public void test_parse_OutputObfuscationMapFilePathNotGiven_ReturnsEmptyCommandOptions(){
        //Arrange
        String[] suppliedCommandOptions = new String[4];
        suppliedCommandOptions[0] = "-inputJsonPath";
        suppliedCommandOptions[1] = "C:\\Dir0";
        suppliedCommandOptions[2] = "-outputJsonPath";
        suppliedCommandOptions[3] = "C:\\Dir1";
        //Act
        CommandOptions commandOptions = _commandOptionsParser.parse(suppliedCommandOptions);

        //Assert
        assertTrue(CommandOptions.isEmpty(commandOptions));
    }

    @Test
    public void test_parse_AllPathsAreGiven_ReturnsNonEmptyCommandOptions(){
        //Arrange
        String[] suppliedCommandOptions = new String[6];
        suppliedCommandOptions[0] = "-inputJsonPath";
        suppliedCommandOptions[1] = "C:\\Dir0";
        suppliedCommandOptions[2] = "-outputJsonPath";
        suppliedCommandOptions[3] = "C:\\Dir1";
        suppliedCommandOptions[4] = "-outputMapPath";
        suppliedCommandOptions[5] = "C:\\Dir2";

        //Act
        CommandOptions commandOptions = _commandOptionsParser.parse(suppliedCommandOptions);

        //Assert
        assertFalse(CommandOptions.isEmpty(commandOptions));
        assertEquals("C:\\Dir0", commandOptions.getInputJsonPath());
        assertEquals("C:\\Dir1", commandOptions.getOutputJsonPath())
        assertEquals("C:\\Dir2", commandOptions.getOutputMapPath())
    }
}
